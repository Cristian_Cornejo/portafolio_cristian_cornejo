import { Code, OpenInNew } from '@material-ui/icons'
import React from 'react'
import projects from '../../projectsData'
const ProjectCard = ({ project }) => {
  const projectRender = projects[project] || null
  return (
    <article className='project__card'>
      <div className='project__card-img-box'>
        <img loading="lazy" src={projectRender.img} alt={projectRender.title} />
      </div>
      <h2 className='project__card-title'> {projectRender.title}</h2>
      <p className='project__card-text'>{projectRender.text}</p>
      <div className='project__links'>
        <div className='project__card-stat'>
          <a href={projectRender.repository} target='_blank' rel='noreferrer'>
          <Code className='project__card-icon'/>
          </a>
        </div>
        <div className='project__card-stat'>
          <a href={projectRender.live} target='_blank' rel='noreferrer'>
            <OpenInNew className='project__card-icon'/>
          </a>
        </div>
        <div className='project__card-stat'>{projectRender.tecs}</div>
      </div>
    </article>
  )
}

export default ProjectCard
