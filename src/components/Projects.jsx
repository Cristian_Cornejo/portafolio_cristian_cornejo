import React from 'react'
import '../styles/projects.css'
import ProjectCard from './shared/ProjectCard'
const Projects = ({ projects }) => {
  return (
    <div id='projects' className='projects section'>
      <div className='container'>
        <h1>Proyectos</h1>
        <div className='line'></div>
        <section className='project__card-list'>
          {projects.map((p, i) => (
            <ProjectCard project={p} key={i} />
          ))}
        </section>
      </div>
    </div>
  )
}

export default Projects
