# PORTAFOLIO WEB

Aplicacion web realizada con React y Bootstrap, Muestra información basica sobre mí, enlaces de contacto y proyectos que he desarrollado.

## DEMO


demo de la aplicación en linea disponible en: [https://portafolio-cris-cor.web.app/](https://portafolio-cris-cor.web.app/)


## Scripts Disponibles
### `npm install`
Primero debemos instalar todas las dependencias con este comando.
### `npm start`

Enciende la aplicación en el puerto [http://localhost:3000](http://localhost:3000)

### `npm run build`

Construye la aplicación en la carpeta `build`.
