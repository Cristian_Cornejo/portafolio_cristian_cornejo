import React from 'react'
import { Code } from '@material-ui/icons'

const CodeListItem = ({ text }) => {
  return (
    <div>
      <p className='home__text'>
        <Code className='about_me__code' />
        {' '+text}
      </p>
    </div>
  )
}

export default CodeListItem
