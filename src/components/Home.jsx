import React from 'react'
import '../styles/home.css'
import '../styles/bubble.css'
import svgs from '../icons'
import { Link } from 'react-scroll'
const Home = ({ technologies }) => {
  const list = technologies || []
  return (
    <div className='banner'>
      <div className='container banner-content'>
        <h2 className='banner__title'>Hola, soy Cristian Cornejo</h2>
        <div className='text-container'>
          <h2 className='banner__text'>Full Stack</h2>
          <h2 className='banner__text'>Software Enginner</h2>
        </div>
        <h3 className='banner__sub-text'>
          con experiencia en Angular, React y Spring.
        </h3>
        <Link
          smooth={true}
          duration={500}
          className='home__projects-link'
          to='projects'
        >
          Proyectos
        </Link>
        <ul>
          {list.map((tech, i) => (
            <li key={i}>
              <svg
                viewBox={svgs[tech].viewBox}
                xmlns='http://www.w3.org/2000/svg'
                className='home__svg'
              >
                {svgs[tech].svg}
              </svg>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default Home
